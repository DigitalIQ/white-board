```
{
      folders: [
        {
          name: 'Blockboards',
          icon: 'assets/icon/folder.png',
          products: [
            { name: 'Product A', img: 'assets/imgs/products/001.jpg', detail: 'Detail here' },
            { name: 'Product B', img: 'assets/imgs/products/002.jpg', detail: 'Detail here' },
            { name: 'Product C', img: 'assets/imgs/products/003.jpg', detail: 'Detail here' },
            { name: 'Product D', img: 'assets/imgs/products/004.jpg', detail: 'Detail here' },
            { name: 'Product E', img: 'assets/imgs/products/005.jpg', detail: 'Detail here' },
            { name: 'Product F', img: 'assets/imgs/products/006.jpg', detail: 'Detail here' },
            { name: 'Product G', img: 'assets/imgs/products/007.jpg', detail: 'Detail here' },
            { name: 'Product H', img: 'assets/imgs/products/008.jpg', detail: 'Detail here' },
          ]
        },
        {
          name: 'Doors',
          icon: 'assets/icon/folder.png',
          products: [
            { name: 'Product 1', img: 'assets/imgs/products/009.jpg', detail: 'Detail here' },
            { name: 'Product 2', img: 'assets/imgs/products/010.jpg', detail: 'Detail here' },
            { name: 'Product 3', img: 'assets/imgs/products/011.jpg', detail: 'Detail here' },
            { name: 'Product 4', img: 'assets/imgs/products/012.jpg', detail: 'Detail here' },
            { name: 'Product 5', img: 'assets/imgs/products/013.jpg', detail: 'Detail here' },
          ]
        },
        {
          name: 'Speciality',
          icon: 'assets/icon/folder.png',
          products: [
            { name: 'Product 1', img: 'assets/imgs/products/014.jpg', detail: 'Detail here' },
            { name: 'Product 2', img: 'assets/imgs/products/015.jpg', detail: 'Detail here' },
            { name: 'Product 3', img: 'assets/imgs/products/016.jpg', detail: 'Detail here' },
            { name: 'Product 4', img: 'assets/imgs/products/017.jpg', detail: 'Detail here' },
            { name: 'Product 5', img: 'assets/imgs/products/018.jpg', detail: 'Detail here' },
          ]
        }

      ]
    };
```